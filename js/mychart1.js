$(document).ready(function(){
var ctx = document.getElementById('myChart1').getContext('2d');
var myChart1 = new Chart(ctx, {
    type: 'doughnut',
    data: {labels: ['Pirmadienis','Antradienis','Trečiadienis', 'Ketvirtadienis', 'Penktadienis', 'Šeštadienis', 'Sekmadienis'],
   
    datasets: [{
         backgroundColor: ['#ff8a65', '#ff7043', '#ff5722', '#f4511e', '#e64a19', '#d84315', '#bf360c'],
         borderColor: '#ff9e80',
         data: [35, 40, 37, 30, 56, 150, 130],
    }],
},
    options: {}
});
 }); 