$(document).ready(function(){
var ctx = document.getElementById('myChart2').getContext('2d');
var myChart2 = new Chart(ctx, {
     type: 'polarArea',
    data: {labels: ['Vokietija','Rusija','Lenkija', 'Ispanija', 'Prancūzija', 'D. Britanija', 'Baltarusija', 'Italija', 'JAV', 'Suomija'],
   
    datasets: [{
         backgroundColor: ['#fbe9e7', '#ffccbc', '#ffab91', '#ff8a65', '#ff7043', '#ff5722', '#f4511e', '#e64a19', '#d84315', '#bf360c'],
         borderColor: 'none',
         data: [12082, 11119, 9345, 6394, 5671, 5068, 4359, 3947, 3590, 2427],

    }],
},
    options: {}
});
 });