$(document).ready(function(){
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
          labels: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],
        datasets: [{
            label: "Lankytojai 2017 metais",
            backgroundColor: 'rgb(255, 128, 0)',
            borderColor: 'rgb(255, 128, 0)',
            data: [6735, 5888, 5981, 10260, 11667, 12712, 13746, 13640, 11567, 9132, 5800, 6690],
        }]
    },

    // Configuration options go here
    options: {}
});
 }); 