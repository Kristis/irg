$(document).ready(function(){
var ctx = document.getElementById('myChart3').getContext('2d');
var myChart3 = new Chart(ctx, {
    // The type of chart we want to create
   
    type: 'line',
  data: {
    labels: ["Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis"],
    datasets: [{ 
        data: [6735, 5888, 5981, 10260, 11667, 12712, 13746, 13640, 11567, 9132, 5800, 6690],
        label: "2017 metai",
        borderColor: "#ff8a65",
        fill: false,
        
      }, { 
        data: [5995,5346,6486,8790,9302,10517,18264,22337,13325,10894,7037,8037],
        label: "2016 metai",
        borderColor: "#ff3d00",
        fill: false,
         }]
     },
  options: {}
});
 }); 